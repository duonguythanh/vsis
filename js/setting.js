$(document).ready(function() {
    var $menu = $("#mainMenu").clone();
    $menu.attr("id", "mainMenuMobile");
    $menu.mmenu({
        dragOpen: {
            open: true
        },
        searchfield: {
            add: true,
            placeholder: "Tìm kiếm các mục...",
            noResults: "Không có mục nào!"
        },
        extensions: ["theme-dark"],
        extensions: ["effect-zoom-menu", "effect-zoom-panels"],
        extensions: ["border-full"],
        extensions: ["pageshadow"]
    });
    var API = $("#mainMenuMobile").data("mmenu");
    $("#button-toggle-menu").click(function (e) {
        API.open();
    });
});

$(function () {
    $(".progressbar li").click(function () {
        var num = $(".progressbar li").index(this);
        $(".tab-content").removeClass('active');
        $(".tab-content").eq(num).addClass('active');
        $(".progressbar li").removeClass('active');
        $(this).addClass('active')
    });
});
$(function () {
    $(".show-card li").click(function () {
        $(".show-card li").removeClass('active');
        $(this).addClass('active')
    });
});
$(function () {
    $(".total-list-number table tr").click(function () {
        $(".total-list-number table tr").removeClass('active');
        $(this).addClass('active')
    });

    $(".table-list tr").click(function () {
        $(".table-list tr").removeClass('active');
        $(this).addClass('active')
    });

    $(".table-user tr").click(function () {
        $(".table-user tr").removeClass('active');
        $(this).addClass('active')
    });
    $(".menu-list").click(function () {
        $(this).toggleClass('active')
        $(".popup-list ul").slideToggle();
    });
    $(".close-x").click(function () {
        $(".note-page").addClass('active')
    });
    $(".open-suport").click(function () {
        $(".btn-demo").toggleClass('close');
        $(".suport-new").toggleClass('active')
    });
    $(".btn-demo").click(function () {
        $(this).toggleClass('close');
        $(".suport-new").toggleClass('active')
    });
    $(".suport-new-close").click(function () {
        $(".btn-demo").toggleClass('close');
        $(".suport-new").toggleClass('active')
    });
    // $(".gridX tr").click(function () {
    //     $(".gridX tr").removeClass('active');
    //     $(this).addClass('active')
    //     $(".open-show").removeClass('active');
    //     $(".show-serach-item").removeClass('active');
    // });
    $(".open-show").click(function () {
        // $(".open-show.active").removeClass('active');
        // $(".show-serach-item.active").removeClass('active');
        // $(this).toggleClass('active');
        // $(this).next('.show-serach-item').toggleClass('active');
    });
});

//bg
$(function(){
    var rand = getRandomArbitrary(1,3);
    var id = rand >9?""+rand:"0"+rand;
    var classId = ".item-"+id;
    $(classId).addClass("active");
});
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



var h1 = $(".show-location").outerHeight();
var h2 = $(".show-step").outerHeight();
var h3 = $(".sec-content").offset().top;
var h4 = h1 + h2 + h3 - 40;
console.log(h4);

jQuery(document).ready(function ($) {
    $(window).on("scroll", function () {
        if($(window).scrollTop()> $(".sec-note").offset().top - $(window).outerHeight()){
            $(".btn-demo").addClass("active");
            // $(".fixd-right").addClass("active2");
        }
        if($(window).scrollTop()> $(".sec-content").offset().top ){
            $(".fill-heading").removeClass("none");
            // $(".fixd-right").addClass("active2");
        }
        if($(window).scrollTop()> h4 ){
            $(".fixd-right .total-list").addClass("active");

        }
        else {
            $(".btn-demo").removeClass("active");
            $(".fill-heading").addClass("none");
            $(".fixd-right .total-list").removeClass("active");
        }
        if($(window).scrollTop()> 200){
            $(".popup-list").addClass("active");
            // $(".fixd-right").addClass("active");
        }
        else {
            $(".popup-list").removeClass("active");
            // $(".fixd-right").removeClass("active");
        }
    });
});



function truncate(substring) {
    if (substring.length > 20)
        return substring.substring(0,20) + '...';
    else
        return substring;
};
$('.show-serach .gridX tr td .fill-txt .fill-txt-odd').each(function(i, obj) {
    var substring = $(this).text();
    var text = truncate(substring);
    $(this).text(text);
});


$(function () {
    $(".list-step li").click(function () {
        var num = $(".list-step li").index(this);
        $(".tab-content").removeClass('active');
        $(".tab-content").eq(num).addClass('active');
        $(".tab-content-img").removeClass('active');
        $(".tab-content-img").eq(num).addClass('active');
        $(".list-step li").removeClass('active');
        $(this).addClass('active')
    });
    // $(".next-step").click(function () {
    //     var index = $(".list-step li").index(".active");
    //     $(".item-step").addClass('active');
    //     $(".item-step").eq( (index+1) % $(".item-step").length ).addClass('active');
    // });
    $(".next-step").click(function() {
        var index = $('.active').index('.item-step');
        var $li = $('.item-step');
        $li.removeClass('active');
        $li.eq( (index+1) % $li.length ).addClass('active');
    });
});

var heading = $(".heading-fixd").html();
$(".fill-heading").html(heading);
$(window).bind('mousewheel', function(event) {
    if (event.originalEvent.wheelDelta >= 0) {
        $(".fill-heading").addClass("active");
        // $(".total-list.active").addClass("active2");
    }
    else {
        $(".fill-heading").removeClass("active");
        // $(".total-list.active").removeClass("active2");
    }
});


